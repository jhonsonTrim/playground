import io.appium.java_client.android.AndroidDriver
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.DesiredCapabilities

import java.net.MalformedURLException
import java.net.URL
import java.util.concurrent.TimeUnit

class TrimaLoginTest {
    private var androidDriver: AndroidDriver<WebElement>? = null
    @Before
    @Throws(MalformedURLException::class)
    fun setUp() {
        androidDriver = AppiumUtils.importAndroidDriver()
        androidDriver?.manage()?.timeouts()?.implicitlyWait(20, TimeUnit.SECONDS)
    }
    @Test
    fun testSuccessLoginScreen() {
        val btnAgreement = androidDriver?.findElementById("btnAgree")
        if (btnAgreement?.isDisplayed == true) btnAgreement.click()
        Thread.sleep(3000)

        val btnLogin = androidDriver?.findElementById("btn_login_userid")
        if (btnLogin?.isDisplayed == true) btnLogin.click()
        Thread.sleep(3000)

        val edtUserName = androidDriver?.findElementById("txtUserName")
        if (edtUserName?.isDisplayed == true) edtUserName.sendKeys("jhonson")

        val edtUserPassword = androidDriver?.findElementById("txtPassword")
        if (edtUserPassword?.isDisplayed == true) edtUserPassword.sendKeys("abc")

        val callLogin = androidDriver?.findElementById("btnLogin")
        if (callLogin?.isDisplayed == true) callLogin.click()
        Thread.sleep(3000)

        edtUserPassword?.clear()
        edtUserPassword?.sendKeys("Trimegah12345")
        if (callLogin?.isDisplayed == true) callLogin.click()
        Thread.sleep(3000)

        val mainActivityShow = androidDriver?.findElementById("container")
        val lytEquityInvestment = androidDriver?.findElementById("lytEquityInvestment")
        if (mainActivityShow?.isDisplayed == true) lytEquityInvestment?.click()
        Thread.sleep(3000)
    }

    @Test
    fun testFailedLoginScreen() {
        val btnAgreement = androidDriver?.findElementById("btnAgree")
        if (btnAgreement?.isDisplayed == true) btnAgreement.click()
        Thread.sleep(3000)

        val btnLogin = androidDriver?.findElementById("btn_login_userid")
        if (btnLogin?.isDisplayed == true) btnLogin.click()
        Thread.sleep(3000)

        val edtUserName = androidDriver?.findElementById("txtUserName")
        if (edtUserName?.isDisplayed == true) edtUserName.sendKeys("jhonson")

        val edtUserPassword = androidDriver?.findElementById("txtPassword")
        if (edtUserPassword?.isDisplayed == true) edtUserPassword.sendKeys("abc")

        val callLogin = androidDriver?.findElementById("btnLogin")
        if (callLogin?.isDisplayed == true) callLogin.click()
        Thread.sleep(3000)

        edtUserPassword?.clear()
        edtUserPassword?.sendKeys("Trimegah123")
        if (callLogin?.isDisplayed == true) callLogin.click()
        Thread.sleep(3000)

        val alertShow = androidDriver?.findElementById("confirm_button")
        if (alertShow?.isDisplayed == true) alertShow.click()
        Thread.sleep(3000)
    }

    @Test
    fun activateBiometricScreen(){
        val btnAgreement = androidDriver?.findElementById("btnAgree")
        if (btnAgreement?.isDisplayed == true) btnAgreement.click()
        Thread.sleep(3000)

        val btnLoginBiometric = androidDriver?.findElementById("layout_biometric")
        if (btnLoginBiometric?.isDisplayed == true) btnLoginBiometric.click()
        Thread.sleep(3000)

        val edtUserName = androidDriver?.findElementById("txtUserName")
        if (edtUserName?.isDisplayed == true) edtUserName.sendKeys("jhonson")

        val edtUserPassword = androidDriver?.findElementById("txtPassword")
        if (edtUserPassword?.isDisplayed == true) edtUserPassword.sendKeys("Trimegah12345")

        val callLogin = androidDriver?.findElementById("btnLogin")
        if (callLogin?.isDisplayed == true) callLogin.click()
        Thread.sleep(3000)

        val activateBiometricDialog = androidDriver?.findElementById("dialog_constraint")
        val btnActivate = androidDriver?.findElementById("tvActivate")
        if (activateBiometricDialog?.isDisplayed == true) btnActivate?.click()
        Thread.sleep(3000)

        val dialogBiometricAgreement = androidDriver?.findElementById("viewBottomSheet")
        val checkBoxConfirmAgreement = androidDriver?.findElementById("cbBioConfirm")
        val submitBiometric = androidDriver?.findElementById("btnBioSubmit")
        if (dialogBiometricAgreement?.isDisplayed == true) checkBoxConfirmAgreement?.click()
        Thread.sleep(1000)

        if (submitBiometric?.isDisplayed == true) submitBiometric.click()
        Thread.sleep(3000)

        val btnBiometricSet = androidDriver?.findElementById("tvOk")
        if (activateBiometricDialog?.isDisplayed == true) btnBiometricSet?.click()
        Thread.sleep(6000)
    }

    @After
    fun end() {
        androidDriver?.quit()
    }
}
