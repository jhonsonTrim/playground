import io.appium.java_client.android.AndroidDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.DesiredCapabilities
import java.net.URL

object AppiumUtils {

    fun importAndroidDriver(): AndroidDriver<WebElement> {
        val androidDriver: AndroidDriver<WebElement>?
        val capabilities = DesiredCapabilities()
        capabilities.setCapability("deviceName", "Nexus 5X API 26")
        capabilities.setCapability("platformName", "Android")
        capabilities.setCapability("plaftformVersion", "8.0")
        capabilities.setCapability("platformName", "Android")
        capabilities.setCapability("appPackage", "com.trimegah.trima.debug")
        capabilities.setCapability("appActivity", "com.trimegah.trima.activities.ActivityDisclaimer")
        capabilities.setCapability("udid", "emulator-5554")
        capabilities.setCapability("automationName", "UiAutomator2")
        androidDriver = AndroidDriver(URL("http://127.0.0.1:4723/wd/hub"), capabilities)
        return androidDriver
    }
}